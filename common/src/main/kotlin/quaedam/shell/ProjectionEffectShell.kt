package quaedam.shell

import net.minecraft.client.gui.components.AbstractSliderButton
import net.minecraft.client.gui.components.CycleButton
import net.minecraft.client.gui.components.StringWidget
import net.minecraft.client.gui.layouts.LayoutElement
import net.minecraft.network.chat.Component
import quaedam.projection.ProjectionEffect
import kotlin.reflect.KMutableProperty0

class ProjectionEffectShell(val effect: ProjectionEffect) {

    interface Provider {

        fun createShell(): ProjectionEffectShell

    }

    val rows = mutableListOf<Row>()
    val width = 150
    val height = 20

    data class Row(val text: Component, val renderer: ShellRenderer)

    fun row(key: String, renderer: ShellRenderer) {
        rows += Row(Component.translatable(key), renderer)
    }

    fun text(key: String, value: Component) = row(key) { StringWidget(value, it.font) }

    fun doubleSlider(key: String, property: KMutableProperty0<Double>, range: ClosedRange<Double>, rawStep: Double) {
        val len = range.endInclusive - range.start
        val step = rawStep / len
        row(key) {
            object : AbstractSliderButton(
                0, 0, width, height,
                Component.literal(String.format("%.2f", property.get())), (property.get() - range.start) / len
            ) {
                override fun updateMessage() {
                    message = Component.literal(String.format("%.2f", property.get()))
                }

                override fun applyValue() {
                    val diff = value % step
                    if (diff < step * 0.5) {
                        value -= diff
                    } else {
                        value += (step - diff)
                    }
                    property.set(range.start + (value * len))
                }
            }
        }
    }

    fun floatSlider(key: String, property: KMutableProperty0<Float>, range: ClosedRange<Float>, rawStep: Float) {
        val len = range.endInclusive - range.start
        val step = rawStep / len
        row(key) {
            object : AbstractSliderButton(
                0,
                0,
                width,
                height,
                Component.literal(String.format("%.2f", property.get())),
                (property.get() - range.start) / len.toDouble()
            ) {
                override fun updateMessage() {
                    message = Component.literal(String.format("%.2f", property.get()))
                }

                override fun applyValue() {
                    val diff = value % step
                    if (diff < step * 0.5) {
                        value -= diff
                    } else {
                        value += (step - diff)
                    }
                    property.set(range.start + (value.toFloat() * len))
                }
            }
        }
    }

    fun intSlider(key: String, property: KMutableProperty0<Int>, range: IntProgression) {
        val len = range.last - range.first
        val step = range.step.toDouble() / len
        row(key) {
            object : AbstractSliderButton(
                0, 0, width, height,
                Component.literal(property.get().toString()), (property.get() - range.first).toDouble() / len
            ) {
                override fun updateMessage() {
                    message = Component.literal(property.get().toString())
                }

                override fun applyValue() {
                    val diff = value % step
                    if (diff < step * 0.5) {
                        value -= diff
                    } else {
                        value += (step - diff)
                    }
                    property.set(range.first + (value * len).toInt())
                }
            }
        }
    }

    fun intCycle(key: String, property: KMutableProperty0<Int>, range: IntProgression) =
        row(key) {
            CycleButton.builder<Int> {
                property.set(it)
                Component.literal(it.toString())
            }
                .displayOnlyValue()
                .withValues(range.toList())
                .withInitialValue(property.get())
                .create(0, 0, width, height, Component.translatable(key))
        }

    fun boolean(key: String, property: KMutableProperty0<Boolean>) =
        row(key) {
            CycleButton.builder<Boolean> {
                property.set(it)
                Component.translatable("$key.$it")
            }
                .displayOnlyValue()
                .withValues(listOf(true, false))
                .withInitialValue(property.get())
                .create(0, 0, width, height, Component.translatable(key))
        }

}

inline fun buildProjectionEffectShell(effect: ProjectionEffect, builder: ProjectionEffectShell.() -> Unit) =
    ProjectionEffectShell(effect).apply(builder)

data class ShellRenderContext(val screen: ProjectionShellScreen) {
    val font get() = screen.getFont()
}

typealias ShellRenderer = (ctx: ShellRenderContext) -> LayoutElement
