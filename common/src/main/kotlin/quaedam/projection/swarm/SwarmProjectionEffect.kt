package quaedam.projection.swarm

import net.minecraft.core.BlockPos
import net.minecraft.nbt.CompoundTag
import net.minecraft.server.level.ServerLevel
import net.minecraft.world.entity.MobSpawnType
import net.minecraft.world.level.levelgen.Heightmap
import quaedam.config.QuaedamConfig
import quaedam.projection.ProjectionEffect
import quaedam.projector.Projector
import quaedam.projector.ProjectorBlockEntity
import quaedam.shell.ProjectionEffectShell
import quaedam.shell.buildProjectionEffectShell
import kotlin.math.min

data class SwarmProjectionEffect(
    var maxCount: Int = 180,
) : ProjectionEffect(), ProjectionEffectShell.Provider {

    companion object {
        const val TAG_MAX_COUNT = "MaxCount"

        val maxMaxCount get() = QuaedamConfig.current.valuesInt["projection.swarm.max_max_count"] ?: 250
    }

    override val type
        get() = SwarmProjection.effect.get()!!

    override fun toNbt(tag: CompoundTag) {
        tag.putInt(TAG_MAX_COUNT, maxCount)
    }

    override fun fromNbt(tag: CompoundTag, trusted: Boolean) {
        maxCount = tag.getInt(TAG_MAX_COUNT)
        if (!trusted) {
            maxCount = min(maxCount, maxMaxCount)
        }
    }

    override fun randomTick(level: ServerLevel, pos: BlockPos) {
        val projector = level.getBlockEntity(pos) as ProjectorBlockEntity
        val entities = level.getEntitiesOfClass(ProjectedPersonEntity::class.java, projector.effectAreaAABB).size
        if (entities < maxCount) {
            val area = projector.effectArea
            for (i in 0..(min(level.random.nextInt(maxCount - entities), 6))) {
                var spawnPos = BlockPos(
                    level.random.nextInt(area.minX(), area.maxX()),
                    area.minY(),
                    level.random.nextInt(area.minZ(), area.maxZ()),
                )
                spawnPos = spawnPos.atY(level.getHeight(Heightmap.Types.WORLD_SURFACE, spawnPos.x, spawnPos.z))
                if (Projector.findNearbyProjections(level, spawnPos, SwarmProjection.effect.get()).isEmpty())
                    continue
                val belowState = level.getBlockState(spawnPos.below())
                val state = level.getBlockState(spawnPos)
                if (belowState.isAir || !belowState.fluidState.isEmpty || !belowState.canOcclude())
                    continue
                if (!state.fluidState.isEmpty)
                    continue
                ProjectedPersonEntity.entity.get().spawn(level, spawnPos, MobSpawnType.TRIGGERED)
            }
        }
    }

    override fun createShell() = buildProjectionEffectShell(this) {
        intSlider("quaedam.shell.swarm.max_count", ::maxCount, 0..maxMaxCount step 5)
    }

}
