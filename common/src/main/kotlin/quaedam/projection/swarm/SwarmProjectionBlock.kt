package quaedam.projection.swarm

import quaedam.projection.EntityProjectionBlock

object SwarmProjectionBlock : EntityProjectionBlock<SwarmProjectionEffect>() {

    override val blockEntity = SwarmProjection.blockEntity

}
